CREATE TABLE IF NOT EXISTS account (
  id INT PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(255),
  last_name VARCHAR(255),
  email VARCHAR(100),
  birthday DATETIME,
  salary INT,
  gender VARCHAR(10)
);

INSERT INTO account (id, first_name, last_name, email, gender, birthday, salary)
VALUES
	(1, 'Margaretha', 'Kiessel', 'mkiessel0@washingtonpost.com', 'F', '2005-10-23', 8324),
	(2, 'Bernetta', 'Anwell', 'banwell1@google.cn', NULL, NULL, 10896),
	(3, 'Darlleen', 'Draper', 'ddraper2@i2i.jp', 'F', '2003-11-26', 1624),
	(4, 'Kiley', 'Judron', 'kjudron3@vk.com', NULL, NULL, 5611),
	(5, 'Ginger', 'Huscroft', 'ghuscroft4@last.fm', 'M', '1988-04-15', 2877),
	(6, 'Ianthe', 'MacDowall', 'imacdowall5@globo.com', 'F', '2009-11-23', 8834),
	(7, 'Lark', 'Roddam', 'lroddam6@jiathis.com', 'F', '2003-01-14', 11243),
	(8, 'Marcelo', 'Astridge', 'mastridge7@hp.com', 'M', '2009-10-10', 2261),
	(9, 'Edouard', 'Rosnau', 'erosnau8@nba.com', NULL, NULL, 1690),
	(10, 'Neal', 'Thomann', 'nthomann9@icq.com', NULL, NULL, 11244),
	(11, 'Fitzgerald', 'Inglesant', 'finglesanta@booking.com', 'M', '2012-04-16', 5782),
	(12, 'Ozzie', 'Menendes', 'omenendesb@sina.com.cn', 'M', '2001-06-07', 5886),
	(13, 'Blancha', 'Osgerby', 'bosgerbyc@ebay.com', 'F', '1986-11-20', 7907),
	(14, 'Nolan', 'Greenhalf', 'ngreenhalfd@usnews.com', 'M', '1987-12-18', 10323),
	(15, 'Wyn', 'Huguenet', 'whuguenete@icio.us', 'M', '2012-04-30', 4719),
	(16, 'Jabez', 'MacLese', 'jmaclesef@upenn.edu', 'M', '2022-10-24', 4993),
	(17, 'Delainey', 'Pennetti', 'dpennettig@typepad.com', 'Polygender', '2022-01-19', 4756),
	(18, 'Mirabella', 'Thys', 'mthysh@jalbum.net', 'F', '2019-05-04', 10122),
	(19, 'Sal', 'Deveraux', 'sdeverauxi@artisteer.com', 'M', '1988-03-06', 4650),
	(20, 'Jorie', 'Tuley', 'jtuleyj@mashable.com', 'F', '2022-07-15', 5383);
