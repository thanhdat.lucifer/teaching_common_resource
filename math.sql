
CREATE TABLE if NOT EXISTS math(
	id INT PRIMARY KEY AUTO_INCREMENT,
	student_name VARCHAR(255),
	age INT,
	score DOUBLE 
);

INSERT INTO math(student_name, age , score)
VALUE 
	('Ngọc',12, 7.5),
	('Thanh',25, 6),
	('Trang',22, 9),
	('Mai',32, NULL),
	('Tùng',19, 6.5),
	('Hải',26, 9),
	('Trường',31, NULL);
