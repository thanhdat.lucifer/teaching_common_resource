CREATE TABLE giao_vien (
	id INT PRIMARY KEY AUTO_INCREMENT,
	ho_ten VARCHAR(50),
	luong FLOAT,
	gioi_tinh VARCHAR(50)
);

CREATE TABLE bo_mon (
	id INT PRIMARY KEY AUTO_INCREMENT,
	ten_bo_mon VARCHAR(50),
	dien_thoai VARCHAR(50),
	truong_bo_mon INT,
	
	FOREIGN KEY (truong_bo_mon) REFERENCES giao_vien(id)
);