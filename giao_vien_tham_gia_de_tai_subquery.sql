CREATE TABLE giao_vien (
	ma_gv int PRIMARY KEY AUTO_INCREMENT,
	ho_ten VARCHAR(255),
	luong DOUBLE,
	gioi_tinh VARCHAR(25),
	dia_chi VARCHAR(255)
);

CREATE TABLE tham_gia_de_tai (
	ma_gv INT,
	ma_dt INT,
	stt INT,
	ket_qua VARCHAR(255)
);

INSERT INTO giao_vien (ho_ten, luong, gioi_tinh, dia_chi)
VALUES
	("Nguyễn Văn Ngọc", 10 , "Nam" , "HN"),
	("Phạm Văn Nam", 12 , "Nam" , "DN"),
	("Trần Văn Hải", 15 , "Nu" , "HP");

INSERT INTO tham_gia_de_tai (ma_gv, ma_dt, stt, ket_qua)
values
	(1, 1, 1, "OK"),
	(3, 1, 2, "OK"),
	(3, 1, 3, "OK"),
	(1, 1, 4, "OK"),
	(3, 1, 5, "OK");